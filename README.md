Translate letters to american sign language.

### Deployed at [https://sondrekindem.gitlab.io/lostintranslation](https://sondrekindem.gitlab.io/lostintranslation)

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn build`

Builds the app for production to the `build` folder.<br />
