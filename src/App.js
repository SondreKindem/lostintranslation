import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import './App.scss';
import Login from "./components/Login";
import Translate from "./components/Translate";
import Profile from "./components/Profile";
import MainNav from "./components/MainNav";
import PrivateRoute from "./components/PrivateRoute";

class App extends React.Component {

    state = {
        user: {
            name: "",
            translations: []
        }
    }

    componentDidMount() {
        const user = JSON.parse(localStorage.getItem("user"));
        console.log(user)
        if (user) {
            this.setState({
                user: user
            })
        }
        console.log(Router.props)
    }

    setName(name) {
        this.setState({
            user: {
                name: name,
                translations: this.state.user.translations,
            }
        }, () => {
            this.saveUser();
        });
    }

    addTranslation(translation){
        const translations = this.state.user.translations;
        if(translations.length === 10){
            translations.pop();
        }
        translations.unshift(translation);

        this.setState({
            user: {
                name: this.state.user.name,
                translations: translations
            }
        }, () => this.saveUser());
    }

    removeTranslation(index){
        // splice the translations array by using a temp var
        const temp = this.state.user.translations;
        temp.splice(index, 1);

        this.setState({
            user: {
                name: this.state.user.name,
                translations: temp
            }
        }, () => this.saveUser());
    }

    saveUser(){
        localStorage.setItem("user", JSON.stringify(this.state.user));
    }

    deleteAll(){
        localStorage.removeItem("user")
        this.setState({
            user: {
                name: "",
                translations: []
            }
        })
    }

    render() {
        return (
            <Router className="App" basename={process.env.PUBLIC_URL}>

                <MainNav user={this.state.user.name}/>

                <hr id="divider"/>

                <Switch>
                    <PrivateRoute exact path="/">
                        <Translate
                            saveTranslation={(translation) => this.addTranslation(translation)}
                            savedTranslations={this.state.user.translations}
                        />
                    </PrivateRoute>
                    <PrivateRoute path="/profile">
                        <Profile
                            username={this.state.user.name}
                            translations={this.state.user.translations}
                            setName={(name) => this.setName(name)}
                            removeTranslation={(index) => this.removeTranslation(index)}
                            deleteAll={() => this.deleteAll()}
                        />
                    </PrivateRoute>
                    <Route path="/login">
                        <Login username={this.state.user.name} createUser={(name) => this.setName(name)}/>
                    </Route>
                </Switch>

            </Router>
        );
    }
}

export default App;
