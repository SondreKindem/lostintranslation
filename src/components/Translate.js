import React, {Component} from "react";
import {Field} from "react-bulma-components/lib/components/form";
import Control from "react-bulma-components/lib/components/form/components/control";
import {Box, Button, Container, Hero} from "react-bulma-components/lib";


class Translate extends Component {

    state = {
        input: "",
        translation: null,
    }

    handleTranslationInput(event) {
        this.setState({
            input: event.target.value,
        });
    }

    pressedEnter(event) {
        if (event.key === "Enter") {
            this.translate();
        }
    }

    translate() {
        if(!this.state.input)
            return;

        // Output image for each character in input string
        const translation = Array.prototype.map.call(this.state.input, (char, index) => {
            if(!char.match(/[A-z]/))
                return (<span key={index} className="spacer">{char}</span>)

            return (<img src={"./signs/" + char.toLowerCase() + ".png"} alt={char} key={index}/>)
        });

        this.props.saveTranslation(this.state.input);

        // Store the image elements in state, so the rendered result can update
        this.setState({
            translation: translation
        });
    }


    render() {
        return (
            <div>
                <Hero color="primary" className="hero-tall">
                    <Hero.Body>
                        <h1 className="is-size-1">Translate!</h1>
                    </Hero.Body>
                </Hero>

                <Container className="centered content-wrapper translation-bar-margin">
                    <Field id="translation-bar" className={`has-addons is-expanded`}>
                        <Control className="is-expanded">
                            <input size="large" className="input is-large is-rounded dropdown-trigger" type="text"
                                   placeholder="Something..."
                                   value={this.state.input}
                                   onKeyDown={(event) => this.pressedEnter(event)}
                                   onChange={(event) => this.handleTranslationInput(event)}
                            />
                        </Control>

                        <Control>
                            <Button color="success" className="is-large is-rounded is-secondary"
                                    onClick={() => this.translate()}>
                                <ion-icon class="icon is-medium" name="send-outline"/>
                            </Button>
                        </Control>
                    </Field>

                    <Box id="result-box" className="has-background-light">
                        <div id="result-box-content">
                            {this.state.translation}
                        </div>
                        <div id="result-box-footer">
                            <h1>Results</h1>
                        </div>
                    </Box>
                </Container>
            </div>
        );
    }
}

export default Translate;
