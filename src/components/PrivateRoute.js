import React, {Fragment} from 'react'
import { Redirect } from 'react-router-dom'

/**
 * Redirect to login if user is not logged in.
 *
 * From https://dev.to/gloriamaris/using-children-props-for-authenticated-routing-in-react-router-v5-and-react-16-9-3e0m
 */

const PrivateRoute = (props) => (
    <Fragment>
        { !!localStorage.getItem("user") ? props.children : <Redirect to={"/login"} /> }
    </Fragment>
)

export default PrivateRoute;
