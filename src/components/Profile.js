import React, {useState} from "react";
import {Button, Container, Hero} from "react-bulma-components/lib";
import Control from "react-bulma-components/lib/components/form/components/control";
import {Field} from "react-bulma-components/lib/components/form";
import {useHistory} from "react-router-dom";

const Profile = (props) => {

    const [username, setUsername] = useState(props.username);

    const history = useHistory();

    function pressedEnter(event) {
        if (event.key === "Enter") {
            setUsername(event.target.value);
            changeName()
        }
    }

    function handleName(event) {
        setUsername(event.target.value);
    }

    function changeName() {
        props.setName(username)
    }

    function removeTranslation(index){
        props.removeTranslation(index);
    }

    function deleteAndLogOut(){
        props.deleteAll();
        history.push("/")
    }

    return (
        <div>
            <Hero color="primary">
                <Hero.Body>
                    <h1 className="is-size-1">Profile</h1>
                </Hero.Body>
            </Hero>
            <Container id="profile-container" className="centered">
                <h3 className="is-size-4">Change name</h3>
                <Field id="name-field" className="has-addons">
                    <Control className="is-expanded">
                        <input onKeyDown={pressedEnter} onChange={handleName} size="large" placeholder="Your new name!" className="input is-large is-rounded" type="text"/>
                    </Control>
                    <Control>
                        <Button className="is-large is-rounded is-secondary" onClick={changeName}>Save</Button>
                    </Control>
                </Field>

                <h3 className="is-size-4">Stored translations</h3>
                <div className="list">
                    {props.translations.length === 0 && <h5 className="is-size-5 has-text-grey">Nothing yet...</h5>}
                    <ul>
                        {props.translations.map((translation, index) => (
                            <div className="list-item" key={index}>
                                <li>
                                    <span>{translation}</span><span onClick={() => removeTranslation(index)} className="button tag is-delete is-secondary delete-btn"/>
                                </li>
                            </div>
                        ))}
                    </ul>
                </div>

                <Button onClick={deleteAndLogOut} color="danger">Delete data & log out</Button>

            </Container>
        </div>
    );
}

export default Profile;
