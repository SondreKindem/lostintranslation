import React from "react";
import {Container, Navbar} from "react-bulma-components/lib";
import NavbarItem from "react-bulma-components/lib/components/navbar/components/item";
import Icon from 'react-bulma-components/lib/components/icon';
import {useHistory} from "react-router-dom";

const MainNav = (props) => {
    const history = useHistory();

    return (
        <Navbar color="primary">
            <Container>
                <Navbar.Brand id="brand">
                    <Navbar.Item onClick={() => history.push("/")}>Lost in Translation</Navbar.Item>
                    <Navbar.Burger/>
                </Navbar.Brand>
                <Navbar.Menu>
                    <Navbar.Container position="end">
                        <NavbarItem onClick={() => history.push("/profile")}>
                            <Icon className="is-medium"><ion-icon class="icon is-medium" name="person-circle-outline"/></Icon>
                            <span>&nbsp;{props.user}</span>
                        </NavbarItem>
                    </Navbar.Container>
                </Navbar.Menu>
            </Container>
        </Navbar>
    );
}

export default MainNav;
