import React, {useEffect} from "react";
import {useHistory} from 'react-router-dom';
import {Button, Container, Hero} from "react-bulma-components/lib";
import {Box} from "react-bulma-components/lib";
import {Field} from "react-bulma-components/lib/components/form";
import Control from "react-bulma-components/lib/components/form/components/control";

const Login = (props) => {
    const history = useHistory();

    let username = "";

    // Can only redirect to main page once the parent's state has been updated
    // Wait for the username prop to be updated, then redirect
    useEffect(() => {
        if (!!localStorage.getItem("user"))
            history.push("/");
    }, [history, props.username]);

    function handleName(event) {
        username = event.target.value;
    }

    function pressedEnter(event) {
        if (event.key === "Enter") {
            saveUser();
        }
    }

    function saveUser() {
        props.createUser(username);
    }

    return (
        <div>
            <Hero color="primary" className="hero-tall">
                <Hero.Body>
                    <h1 className="is-size-1">Please introduce yourself</h1>
                </Hero.Body>
            </Hero>

            <Container className="centered content-wrapper">
                <Box id="login-box">
                    <Field className="has-addons">
                        <Control className="is-expanded">
                            <input size="large" className="input is-large is-rounded" type="text"
                                   placeholder="What is your name?" onKeyDown={pressedEnter} onChange={handleName}/>
                        </Control>
                        <Control>
                            <Button className="is-large is-rounded is-secondary" onClick={saveUser}>Save</Button>
                        </Control>
                    </Field>
                </Box>
            </Container>
        </div>
    );

}

export default Login;
